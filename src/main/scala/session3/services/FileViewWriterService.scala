package session3.services

import java.io.{BufferedWriter, File, FileWriter}

import session3.models.View

/*
Classes in Scala are blueprints for creating objects.
They can contain methods, values, variables, types, objects, traits, and classes which are collectively called members.
In this case FileViewWriterService is overriding the functionality write(view: View) of his parent trait  ViewWriter
https://docs.scala-lang.org/tour/classes.html
*/
class FileViewWriterService() extends ViewWriter {
  override def write(view: View): Unit = {
    val file = new File(s"${view.name()}.txt")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(view.toString())
    bw.close()
    Console.println("File created on: " + file.getAbsolutePath)
  }
}
