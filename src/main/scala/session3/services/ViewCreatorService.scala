package session3.services

import session3.models.{MostCommonBirdView, Sighting, TotalAmountView, UniquesBirdTypeDetectedView}

object ViewCreatorService {
  def createTotalAmountView(inputSightingSeq: Seq[Sighting]): TotalAmountView = {
    //Creating an returning a case class TotalAmountView object
    return TotalAmountView(inputSightingSeq.size)
  }

  def createUniquesBirdTypeDetectedView(inputSightingSeq: Seq[Sighting]): UniquesBirdTypeDetectedView = {
    var uniques: Set[Int] = Set()
    for (sighting <- inputSightingSeq) {
      if (!uniques.contains(sighting.birdTypeId)) {
        uniques += sighting.birdTypeId
      }
    }
    //Creating an returning a case class UniquesBirdTypeDetectedView object
    return UniquesBirdTypeDetectedView(uniques.size)
  }

  def createMostCommonBirdView(inputSightingSeq: Seq[Sighting]): MostCommonBirdView = {

    //Grouping by birdTypeId
    val groupsByBirdTypeId: Map[Int, Seq[Sighting]] = inputSightingSeq.groupBy(sighting => sighting.birdTypeId)

    //Convecting the previous grouping onto Map of (typeBirdId, count)
    val uniquesCount: Map[Int, Int] = groupsByBirdTypeId.map(par => (par._1, par._2.size))

    //Calculating the max by the the count filed on the previous Map
    val max = uniquesCount.maxBy(par => par._2)

    //Creating an returning a case class UniquesBirdTypeDetectedView object
    return MostCommonBirdView(max._1, max._2)
  }
}
