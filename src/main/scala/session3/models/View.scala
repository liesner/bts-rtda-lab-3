package session3.models

/*
  Abstract types, such as traits and abstract classes, can in turn have abstract type members.
  This means that the concrete implementations (MostCommonBirdView, TotalAmountView and UniquesBirdTypeDetectedView)
  define the actual types. It also implies that the concrete types need to overriding all declared but undefined
  funtionalities on the class: toString() and name()
  https://docs.scala-lang.org/tour/abstract-type-members.html
*/
abstract class View {
  def toString():String
  def name(): String
}
