package session3.models

/*
  Case classes are good for modeling immutable data.
  Once you create a TotalAmountView object it will never change.
  TotalAmountView is modeling an pre-calculated view of the data.
  It's overriding the functionalities of the abstract class View
  https://docs.scala-lang.org/tour/case-classes.html
*/
case class TotalAmountView(total: Int) extends View {
  override def toString: String = {
    s"Total sightings: $total"
  }

  override def name(): String = "total_amount_view"
}
