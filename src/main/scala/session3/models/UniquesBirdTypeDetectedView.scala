package session3.models

/*
  Case classes are good for modeling immutable data.
  Once you create a UniquesBirdTypeDetectedView object it will never change.
  UniquesBirdTypeDetectedView is modeling an pre-calculated view of the data.
  It's overriding the functionalities of the abstract class View
  https://docs.scala-lang.org/tour/case-classes.html
*/
case class UniquesBirdTypeDetectedView(uniques: Int) extends View {
  override def toString: String = {
    s"Uniques Bird Type Detected: $uniques"
  }

  override def name(): String = "uniques_birdType_detected_view"
}
