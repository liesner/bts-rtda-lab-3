package session3.models

/*
  Case classes are good for modeling immutable data.
  Once you create a Sighting object it will never change.
  Sighting is modeling an observation witch is immutable.
*/
case class Sighting(birdTypeId: Int, countryName: String) {

}
