package session3.cli

import session3.models.{Sighting, View}
import session3.services.{ConsoleViewWriterService, FileViewWriterService, TransformService, ViewCreatorService, ViewWriter}

import scala.io.Source

object BirdInsightViewCreatorApp {
  var inputFileName: String = "sightings.txt"
  var outputTo: String = "console"

  def main(args: Array[String]): Unit = {
    //The app use "sightings.txt"  and "console" as default args
    if(args.size > 0)
     inputFileName = args(0)

    if(args.size > 1)
      outputTo = "file"

    //Read from external file
    val lines = Source.fromFile(inputFileName).getLines().toSeq

    //Transform input lines into Seq[Sighting]
    val sightings: Seq[Sighting] = TransformService.execute(lines)

    var views: List[View] = List[View]()

    /*
        Create the view using ViewCreatorService singleton object. Adding them to views List
     */
    views ::= ViewCreatorService.createTotalAmountView(sightings)
    views ::= ViewCreatorService.createUniquesBirdTypeDetectedView(sightings)
    views ::= ViewCreatorService.createMostCommonBirdView(sightings)

    var writer: ViewWriter = null

    //Selecting the view writing service
    if(outputTo.equals("console"))
      writer = new ConsoleViewWriterService()
    else
      writer = new FileViewWriterService()

    //Writing all views using writing service selected
    for (view <- views) {
      writer.write(view)
    }
  }
}
